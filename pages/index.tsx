import Header from "../components/Header"
import ProductList from "../components/ProductList"
import { IProduct } from "../components/Product"
import Footer from "../components/Footer"
import Contact from "../components/Contact"
import Head from "next/head"

import "../styles.scss"

interface IIndexProps {
  products: IProduct[]
}

const Index = (props: IIndexProps) => {
  return (
    <div className="app">
      <Head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdn.snipcart.com/scripts/2.0/snipcart.js" data-currency="eur" data-api-key="YTMyOGQxMWMtZjM0OC00NWVlLWJhZWMtN2QxZTA2OWJkNjFhNjM3NDM0NzAyNTQ2NjQwODU3" id="snipcart"></script>
        <link href="https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="/static/favicon.ico" />
      </Head>
      <Header />
      <main className="main">
        <img src="/static/aquarium.svg" alt="a" className="background-image" />
        <div className="promotional-message">
          <h3>DISCOVER</h3>
          <h2>Italian Hot Peppers</h2>
          <p>An <strong>exclusive collection of sauces</strong> available for everyone.</p>
        </div>
        <ProductList products={props.products} />
        <Contact />
      </main>
      <Footer />
    </div>
  )
}

Index.getInitialProps = async () => {
  return {
    products: [
      {id: 'nextjs_redhot', name: 'Hot Chilli Pepper Sauce', price: 25.00, image: '../static/red-peppers.jpg', description: 'The red hot pepper is arguably one of the hottest in the world. It is recognized by its long tail that can curve up to 180 degrees. We use this to make a hot and spicy sauce that you can use for any kind of recipe. Available to buy in a small 250g bottle'} as IProduct,
      {id: "nextjs_superhot", name: "Super Hot Pepper Sauce", price: 35, image: "../static/Pot-Douglah-Pepper.jpg",description: "Superhot chili peppers go beyond habanero pepper heat and surpass 350,000 Scoville Heat Units. There are a number of varieties of superhot chili peppers and have recently topped over 2 Million Scoville Heat Units with the Carolina Reaper. Treat these peppers with respect as you cook with them. We did and came up with this super hot sauce ina a small 250g bottle."} as IProduct,
      {id: "nextjs_sweethot", name: "Sweet Hot Pepper Sauce", price: 7.50, image: "../static/Datil-Peppers.jpg", description: "The datil pepper is a fiery chili produced mainly in St. Augustine, Florida. The pods are smallish and yellow-orange when mature. They reach up to 300,000 Scoville Heat Units. The Datil pepper packs the intense heat of a habanero or a Scotch Bonnet, but its flavor is sweeter, and more ... we made a sauce from it that you can buy in a small 250g bottle"} as IProduct,
      {id: "nextjs_fammilypack", name: "Family Pack", price: 5.00, image: "../static/bottles.jpg", description: "We sell all our sauces in bigger 1 litre bottles in an economic family packe of 3 bottles (1 Sweet, 1 Hot, 1 Super-Hot) for a discount of over 40%."} as IProduct,
    ]
  }
}

export default Index
