export default {
  title: 'Product',
  name: 'product',
  type: 'document',
  fields: [
    {
      type: 'string',
      title: 'Name',
      name: 'name',
      description: 'Product Name',
      validation: Rule => Rule.required()
    },
    {
      type: 'string',
      title: 'Description',
      name: 'desc',
      description: 'Product Description',
      validation: Rule => Rule.required()
    },
    {
      type: 'number',
      title: 'ID',
      name: 'id',
      description: 'Product ID',
      validation: Rule => Rule.required()
    },
    {
      type: 'number',
      title: 'Price',
      name: 'price',
      description: 'Product Price (Euro)',
      validation: Rule => Rule.required()
    },
    {
      type: 'url',
      title: 'Product URL',
      name: 'product_url',
      description: 'Product URL',
      validation: Rule => Rule.required()
    },
    {
    type: 'image',
    title: 'Product Image',
    name: 'product_image',
    description: 'Product Image',
    validation: Rule => Rule.required()
    },
  ]
}
