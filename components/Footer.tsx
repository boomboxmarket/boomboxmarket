export default function Footer(){
  return (
    <footer className="footer">
      <p>
        Follow us on the follwing Social Networks or contact us using the above button.
        <div className="footer__left">
          <img src="/static/twitter-25x25.png"/>
          <img src="/static/facebook-25x25.png"/>
          <img src="/static/instagram-25x25.png"/>
            <img src="/static/linkedin-25x25.png"/>
        </div>
      </p>
    </footer>
  )
}
